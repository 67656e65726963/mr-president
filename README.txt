=== Toy Title ===
Contributors: thewhodidthis
Tags: wp_title, document_title_parts
Requires at least: 3.0.1
Tested up to: 5.5
Stable tag: 0.0
License: Unlicense
License URI: http://unlicense.org/

Slightly edits `wp_title` defaults.
