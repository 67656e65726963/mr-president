<?php

/**
 * Plugin Name:       Toy Title
 * Plugin URI:        https://gitlab.com/67656e65726963/toy-title
 * Description:       Reformats the document title.
 * Version:           0.0.0
 * Author:            67656e65726963
 * Author URI:        https://gitlab.com/67656e65726963
 * License:           Unlicense
 * License URI:       http://unlicense.org/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_filter(
	'document_title_parts',
	function( $title ) {
		if ( is_search() ) {
			/* translators: %s: search query. */
			$title['title'] = sprintf( esc_html__( 'Search for %s' ), get_search_query() );
		}

		return apply_filters( 'toy_document_title_parts', $title );
	},
	67
);

add_filter(
	'document_title_separator',
	function() {
		return '\\';
	},
	67
);

add_filter(
	'wp_title',
	function( $title ) {
		return is_404() ? __( 'Not found' ) : $title;
	},
	67
);
